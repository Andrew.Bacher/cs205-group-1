import csv
import re
import sqlite3

# Begin Main
def main():
    stop = 0  # exit variable

    # Set up database connection
    db = sqlite3.connect('songs.db')

    # Create cursor to access database
    global cursor
    cursor = db.cursor()


    print('\nWelcome! Type your queries below. \nType "quit" or "exit" to exit. Type "help" for help.')
    # Main loop for prompting user
    while(stop != 1):
        user_input = input(">> ")  # get user input

        if(user_input.lower() == 'quit' or user_input.lower() == 'exit'):
            stop = 1
        elif(user_input.lower() == 'help'):
            print('----------------------HELP----------------------\n')
            print('Query Structure: {Identifier} <Subject> [Action] \n')
            print('Identifiers: Artist,Title,Songs,Top \n')
            print('{Artist} <stage name> [first,last,full, nationality] \n')
            print('{Artist} <full name> [stage, nationality] \n')
            print('{Title} <song title> [full, nationality, dur, year, artist]\n')
            print('{Songs} <stage name>\n')
            print('{Top} <year>\n')
            print('*type "load data" to load data; type "quit" or "exit" to exit*\n')
            print('**If subject is multiple words, put DOUBLE quotes ("") around it**\n')
            print('***Make sure you spell your subject corretly (no added spaces) and use capitals where necessary***\n')
            print('------------------------------------------------')
        elif (user_input.lower() == 'load data'):
            load_data()
            db.commit()
            print('Data loaded successfully.')
        elif(user_input.split(" ")[0].lower()=='artist' or user_input.split(" ")[0].lower()=='title' or user_input.split(" ")[0].lower()=='top' or user_input.split(" ")[0].lower()=='songs'):
            parse(user_input)
        else:
            print('Error: "'+user_input+'" not recognized. Try again')
    print('Goodbye :)')
# End Main

# Begin read_from_csv
def read_from_CSV(filename, table):
    """
    read_from_csv function will read in data from a CSV file and insert the data into an
    appropriate database table. Each column must correspond to each attribute in the correct
    order for it to work correctly. The 'insert' query executes one row at a time to account
    for potential duplicates.

    :param filename: CSV file to read in data from
    :param table: database table to inser data into
    :return: void
    """
    insert_query = "INSERT OR IGNORE INTO " + table + " VALUES "

    with open(filename, newline='') as file:
        reader = csv.reader(file)

        # Skip header row
        next(reader)

        for row in reader:
            row_query = insert_query + str(tuple(row))
            cursor.execute(row_query)
# end read_from_csv

# Begin load_data
def load_data():
    # Queries to create tables
    create_artists = '''CREATE TABLE IF NOT EXISTS tblArtists (
                            pmkStageName   VARCHAR (30) PRIMARY KEY NOT NULL,
                            fldFirstName   VARCHAR (30) NOT NULL,
                            fldLastName    VARCHAR (30) NOT NULL,
                            fldNationality VARCHAR (15) NOT NULL
                        );'''

    create_tracks = '''CREATE TABLE IF NOT EXISTS tblTracks (
                        pmkTrackId      INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,
                        fldTitle        VARCHAR (50) NOT NULL,
                        pfkStageName    VARCHAR (30) REFERENCES tblArtists (pmkStageName)
                                                    ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
                        fldYear     SMALLINT (4) NOT NULL,
                        fldLength   SMALLINT (3) NOT NULL
                    );'''

    cursor.execute(create_artists)
    cursor.execute(create_tracks)

    # Load in data
    read_from_CSV('artists.csv', 'tblArtists')
    read_from_CSV('songs.csv', 'tblTracks')
# End load_data

# Begin parse
def parse(user_input):
    """
            This function is responsible for parsing through user input and identifying id, subject, and action
            to be used in do_query(). Also does initial error checks and provides user feedback

            :param user_input       user input from main

            :return Zero when the query executes successfully, non-zero otherwise

        """
    subject = (re.findall(r'"(.*?)"', user_input)) # Stores multi word subject

    if(len(subject) ==0):  # if the subject not in double quotes
        parsed = user_input.split(" ")  # split user input by spaces
        identifier = parsed[0].lower()  #set the first word as the identifier

        if(identifier == 'top' or identifier == 'songs'):  # if the query is a top or songs query

            if(len(parsed)==2):  # make sure the parsed input is proper length
                subject = parsed[1]  # set the subject to be the second word in the string
                do_query(identifier,subject,'')  # send parameters to do_query (action = blank string)
            else:  # if improper length, return 1
                print('Error: Invalid Query. Please try again. Type "HELP" for help')
                return 1

        elif(len(parsed)==3):  # for all other valid queries
            subject = parsed[1]  # set subject to be second word in string
            do_query(identifier,subject,parsed[2].lower())  # send parameters to do_query

        else:  # if invalid length of query
            print('Error: Invalid Query. Please try again. Type "HELP" for help')
            return 1

    elif (user_input.split(" ")[0].lower() == "songs"):
        # this section is for queries of the form >> songs "Maroon 5", which otherwise arent recognized by the parser
        if (len(subject[0].split(" ")) + 1 == len(user_input.split(" "))):
            # this if statement catches queries such as >> songs "Maroon 5" Adele, which otherwise would make it through
            parsed = user_input.split(" ")
            identifier = parsed[0].lower()
            do_query(identifier, subject[0], '')
        else:  # for invalid queries of the form >> songs "stage name" with extra words after "stage name"
            print('Error: Invalid Query. Please try again. Type "HELP" for help')
            return 1

    else:  # multi-word subject
        user_input = user_input.replace(subject[0],'')  # replace the subject with nothing
        parsed = user_input.split(" ")  # split user input by spaces
        identifier = parsed[0].lower()  # set first word as identifier
        if (len(parsed) == 3 and "" != parsed[2]):  # for all valid queries
            do_query(identifier, subject[0], parsed[2].lower())  # send parameters to do_query

        else:  # if invalid query
            print('Error: Invalid Query. Please try again. Type "HELP" for help')
            return 1

    return 0
# End parse

def do_query(id,subject,action):
    """
        This function is responsible for validating and executing queries based on user input given to
        the parser function.

        :param id       Identifies which data is given in the query.
        :param subject  The actual data of type (id) supplied in the query
        :param action   Which type of query to execute with the given data

        :return Zero when the query executes successfully, non-zero otherwise

    """

    # first off we check to see if the databases exist
    cursor.execute('SELECT name from sqlite_master where type= "table" AND name = "tblTracks"')
    results = cursor.fetchall()#store the results of query

    cursor.execute('SELECT name from sqlite_master where type= "table" AND name = "tblArtists"')
    results2 = cursor.fetchall()  # store the results of query

    if(not(results and results2)):#if there are no results: Load data
        print("Looks like you've forgotten to load your data, try the 'load data' command")
        return 1

    # initial if statements check for valid identifier

    # ARTIST Identifier
    if (id == 'artist'):

        # Action = nationality
        if (action == 'nationality'):
            query = '''SELECT fldNationality FROM tblArtists                                    
                                         WHERE pmkStageName = \'%s\'''' % subject
            cursor.execute(query)

            results = cursor.fetchall()
            if (not (results) and len(
                    subject.split(" ")) > 1):  # if the query comes up with nothing, the subject must be a full name
                query = "SELECT fldNationality FROM tblArtists WHERE fldFirstName = '" + subject.split(" ")[0] + "' "
                query += "AND fldLastName = '" + subject.split(" ")[1] + "'"
                cursor.execute(query)

                results = cursor.fetchall()
            if (not (results)):  # if this query comes back empty, then there is no record of subject in db
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1

            print('<< '+results[0][0])

        #Action = Full
        elif (action == 'full'):
            query = '''SELECT fldFirstName,fldLastName FROM tblArtists                                    
                                         WHERE pmkStageName = \'%s\'''' % subject
            cursor.execute(query)

            results = cursor.fetchall()
            if (not (results)):
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1

            print('<< '+results[0][0] + ' ' + results[0][1])


        # Action = first
        elif (action == 'first'):
            query = '''SELECT fldFirstName FROM tblArtists 
                                            WHERE pmkStageName = \'%s\''''%subject
            cursor.execute(query)

            results = cursor.fetchall()
            if (not (results)):
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1

            print('<< '+results[0][0])


        # Action = last
        elif (action == 'last'):
            query = '''SELECT fldLastName FROM tblArtists 
                                            WHERE pmkStageName = \'%s\''''%subject
            cursor.execute(query)

            results = cursor.fetchall()
            if (not (results)):
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1

            print('<< '+results[0][0])


        # Action = stage
        elif (action == 'stage'):
            if (len(subject.split(" ")) > 1):
                query = "SELECT pmkStageName FROM tblArtists WHERE fldFirstName = '" + subject.split(" ")[0] + "' "
                query += "AND fldLastName = '" + subject.split(" ")[1] + "'"
                cursor.execute(query)
                results = cursor.fetchall()
                if (not (results)):
                    print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                    return 1

                print('<< ' + results[0][0])
            else:
                print("Error: \"%s\" does not exist in the database. Must be more than one word." % subject)


        # Action not recognized
        else:
            print('Error: Invalid Action. Please try again. Type "HELP" for help')
            return 1

        # TITLE identifier
    elif(id =='title'):
        # Returns the full name of the artist associated with the given title
        if (action == 'full'):
            query = '''SELECT fldFirstName, fldLastName FROM tblArtists
                            JOIN tblTracks ON pmkStageName = pfkStageName
                            WHERE fldTitle = \'%s\'''' % subject
            cursor.execute(query)

            full_name = cursor.fetchall()
            if not full_name:
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1
            else:
                print('<< '+full_name[0][0] + ' ' + full_name[0][1])

        # Returns the nationality of the artist associated with the given song title
        elif (action == 'nationality'):
            query = '''SELECT fldNationality FROM tblArtists
                    JOIN tblTracks ON pmkStageName = pfkStageName
                    WHERE fldTitle = \'%s\'''' % subject
            cursor.execute(query)

            nationality = cursor.fetchall()
            if not nationality:
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1
            else:
                print('<< '+nationality[0][0])

        # Returns the duration of the song with the given song title
        elif (action == 'dur'):
            query = "SELECT fldLength FROM tblTracks WHERE fldTitle = '%s'" % subject
            cursor.execute(query)

            duration = cursor.fetchall()
            if not duration:
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1
            else:
                print('<< '+str(duration[0][0]))

        # Returns the year released for the given song title
        elif (action == 'year'):
            query = "SELECT fldYear FROM tblTracks WHERE fldTitle = '%s'" % subject
            cursor.execute(query)

            year = cursor.fetchall()
            if not year:
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1
            else:
                print('<< '+str(year[0][0]))

        # Returns the artist associated with the given song title
        elif (action == 'artist'):
            query = "SELECT pfkStageName FROM tblTracks WHERE fldTitle = '%s'" % subject
            cursor.execute(query)

            artist = cursor.fetchall()
            if not artist:
                print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
                return 1
            else:
                print('<< '+artist[0][0])

        else:
            print('Error: Invalid Action. Please try again. Type "HELP" for help')
            return 1

    # SONGS identifier
    elif(id == 'songs'):
        #print('songs '+id)
        query = "SELECT fldTitle FROM tblTracks WHERE pfkStagename = '%s'" % subject
        cursor.execute(query)
        song_results = cursor.fetchall()
        # when nothing is found, return an informational message
        if not song_results:
            print("Error: \"%s\" does not exist in the database. Please check your spelling/capitalization." % subject)
            return 1
        else:
            # we often have multiline returns here, use a for loop to print them all
            for i in range(0, len(song_results)):
                print("<< " + song_results[i][0])

    # TOP identifier
    elif(id == 'top'):

        if(subject.isnumeric()):
            print('top ' + subject)
            query = "SELECT fldTitle FROM tblTracks WHERE fldYear = '%s'" % subject
            cursor.execute(query)

            results = cursor.fetchall()

            if (not (results)):
                print('Error: No songs for selected year. Please try again. Type "HELP" for help')
                return 1
            for result in results:
                s = "<< "
                for i in range(0, len(result)):
                    cursor.execute("SELECT pfkStageName FROM tblTracks WHERE fldTitle = \"%s\"" % result[i])
                    artist = cursor.fetchall()
                    s += result[i] + " -- " + artist[0][0]
                print(s)

        else:
            print('Error: Invalid Subject. Must be a year. Please try again. Type "HELP" for help')
            return 1

    # Unrecognized Identifier
    else:
        print('Somehow, your identifier is not recognized...')
        return 1

    return 0  # if all goes well, return 0
# End do_query

main()
